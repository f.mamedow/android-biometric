package fm.android.androidbiometric.utils

import androidx.biometric.BiometricPrompt

class AuthCallback(
    private val onError: (message: String) -> Unit,
    private val onSuccess: () -> Unit,
    private val onFailed: () -> Unit,
) : BiometricPrompt.AuthenticationCallback() {
        override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
            onError(errString.toString())
        }

        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
            onSuccess()
        }

        override fun onAuthenticationFailed() {
            onFailed()
        }
    }