package fm.android.androidbiometric

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isVisible
import fm.android.androidbiometric.utils.AuthCallback
import fm.android.androidbiometric.utils.BiometryHelper

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    override fun onResume() {
        super.onResume()
        checkBiometry()
    }

    private fun checkBiometry() {
        when(BiometryHelper.getBiometryState(this)) {
            BiometryHelper.BiometricState.ENABLED -> {
                biometryAvailableState()
            }
            BiometryHelper.BiometricState.UNAVAILABLE,
            BiometryHelper.BiometricState.DISABLED -> {
                biometryUnAvailableState()
            }
        }
    }

    private fun biometryUnAvailableState() {
        val biometryBtn : Button = findViewById(R.id.biometry_btn)
        val errorText : TextView = findViewById(R.id.tv_biometry_unavailable)

        biometryBtn.setOnClickListener {
            startActivity(BiometryHelper.getIntentForInitBiometry())
        }
        errorText.isVisible = true
    }

    private fun biometryAvailableState() {
        val biometryBtn : Button = findViewById(R.id.biometry_btn)
        val errorText : TextView = findViewById(R.id.tv_biometry_unavailable)

        biometryBtn.setOnClickListener {
            showBiometryPrompt()
        }
        errorText.isVisible = false
    }

    private fun showBiometryPrompt() {
        val successfullyTextView : TextView = findViewById(R.id.tv_successfully_auth)
        successfullyTextView.isGone = true

        val callback = AuthCallback(
            onSuccess = ::onAuthSuccess,
            onError = ::onAuthError,
            onFailed = ::onAuthFailed
        )
        BiometryHelper.showBiometry(this, callback)
    }

    private fun onAuthSuccess() {
        val successfullyTextView : TextView = findViewById(R.id.tv_successfully_auth)
        successfullyTextView.isVisible = true
    }

    private fun onAuthError(message: String) {
        val errorText : TextView = findViewById(R.id.tv_biometry_unavailable)
        errorText.isVisible = true
        Log.e("Biometric Auth" ,"message: $message")
    }

    private fun onAuthFailed() {
        val successfullyTextView : TextView = findViewById(R.id.tv_successfully_auth)
        successfullyTextView.isVisible = false
    }

}